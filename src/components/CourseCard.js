import { useState, useEffect } from "react";

import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

// Destructure the "courseProp" frmo the prop parameter
// CourseCard(prop)
export default function CourseCard({ courseProp }) {
    // console.log(props.courseProp.name)
    // console.log(typeof props)
    // console.log(courseProp);

    // Scenario: Keep track the number of enrollees of each course

    // Destructure the course properties into their own variables
    const { _id, name, description, price, slots } = courseProp;

    /* 
        Syntax:
            const [stateName, setStateName] = useState(initialStateValue);
            - Using the state hook, it returns an array with the following elements
                - first element contains the current initial State value
                - second element is a setter function that is used to change the value of the first element 
    */

    // const [count, setCount] = useState(0);
    // // console.log(useState(10));

    // const [seats, setSeats] = useState(30);

    // Function that keeps track of the enrollees for each course
    /* 
        We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button when the seats reach 0
    */
    /* 
        Syntax:
            useEffcet(function, [dependencyArray])
    */

    // const [disabled, setDisabled] = useState(false);

    /* function unEnroll() {
        setCount(count - 1);
        console.log(`Enrollees: ${count}`);
        setSeats(seats + 1);
        console.log(`Enrollees: ${seats}`);
    } */

    // function enroll() {
    //     // Activity Solution
    //     /* 
    //             if (seats > 0) {
    //                 setCount(count + 1);
    //                 console.log(`Enrollees: ${count}`);
    //                 setSeats(seats - 1);
    //                 console.log(`Enrollees: ${seats}`);
    //             } else {
    //                 alert("No more seats available");
    //             }
    //      */
    //     setCount(count + 1);
    //     console.log(`Enrollees: ${count}`);
    //     setSeats(seats - 1);
    //     console.log(`Enrollees: ${seats}`);
    // }

    // useEffect(() => {
    //     if (seats <= 0) {
    //         setDisabled(true);
    //         alert("No more seats available");
    //     }
    // }, [seats])

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>
                    {price}
                </Card.Text>
                <Card.Subtitle>
                    Slots:
                </Card.Subtitle>
                <Card.Text>
                    {slots} enrollees
                </Card.Text>
                <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
                {/* <Button variant="danger" onClick={unEnroll} className="ms-2">Unenroll</Button> */}
            </Card.Body>
        </Card>
    )
}