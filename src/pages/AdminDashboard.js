import { useContext, useEffect, useState } from "react";

import { Navigate } from 'react-router-dom';


import { Button, Table } from "react-bootstrap";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDashboard() {

    const { user } = useContext(UserContext);

    // Create allCourses state to contain the courses from the response of our fetch data
    const [allCourses, setAllCourses] = useState([]);

    // fetchData() function to get all the active/inactive courses
    const fetchData = () => {
        // get all the courses from the database
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setAllCourses(data.map(course => {
                    return (
                        <tr key={course._id}>
                            <td>{course._id}</td>
                            <td>{course.name}</td>
                            <td>{course.description}</td>
                            <td>{course.price}</td>
                            <td>{course.slots}</td>
                            <td>{course.isActive ? "Active" : "Inactive"}</td>
                            <td>
                                {
                                    // Conditional rendering on what button should be visible based on the status of the course
                                    (course.isActive)
                                        ?
                                        <Button variant="danger" size="sm" onClick={() => archive(course._id, course.name)}>Archive</Button>
                                        :
                                        <>
                                            <Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(course._id, course.name)}>Unarchive</Button>
                                            <Button variant="secondary" size="sm" className="mx-1">Edit</Button>
                                        </>
                                }
                            </td>
                        </tr>
                    )
                }))
            });
    }

    // [SECTION] Setting the course to Active/Inactive

    // Making the course inactive
    const archive = (id, courseName) => {
        console.log(id);
        console.log(courseName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Archive Successful",
                        icon: "success",
                        text: `${courseName} is now inactive`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    fetchData();
                } else {
                    Swal.fire({
                        title: "Archive Failed",
                        icon: "error",
                        text: `Something went wrong. Please try again later.`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            })
    }

    // Making the course active
    const unarchive = (id, courseName) => {
        console.log(id);
        console.log(courseName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/courses/archive/${id}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Unarchive Successful",
                        icon: "success",
                        text: `${courseName} is now active`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                    fetchData();
                } else {
                    Swal.fire({
                        title: "Unarchive Failed",
                        icon: "error",
                        text: `Something went wrong. Please try again later.`,
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            })
    }

    // To fetch all the courses in the first render of the page
    useEffect(() => {
        fetchData();
    }, [])

    return (
        (user.isAdmin)
            ?
            <>
                {/* Header for the admin dashboard and functionality for create course and show enrollments */}
                <div className="mt-5 mb-3 text-center">
                    <h1>Admin Dashboard</h1>
                    {/* Adding a new course */}
                    <Button variant="success" className="mx-2">Add Course</Button>
                    {/* To view all the user enrollments */}
                    <Button variant="secondary" className="mx-2">Show Enrollments</Button>
                </div>

                {/* For viewing all the courses in the database */}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Course ID</th>
                            <th>Course Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Slots</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allCourses}
                    </tbody>
                </Table>
            </>
            :
            <Navigate to="/courses" />
    )
}