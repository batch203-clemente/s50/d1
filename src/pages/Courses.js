import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import CourseCard from '../components/CourseCard';
import UserContext from '../UserContext';
// import coursesData from '../data/coursesData';

export default function Courses() {

    const { user } = useContext(UserContext);

    // Course state that will be used to store the courses retrieved in the database
    const [courses, setCourses] = useState([]);

    useEffect(() => {
        // Will retrieve all the active courses
        fetch(`${process.env.REACT_APP_API_URL}/courses/`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setCourses(data.map(course => {
                    return (
                        <CourseCard key={course._id} courseProp={course} />
                    )
                }))
            })
    }, [])

    // console.log(coursesData);
    // console.log(coursesData[0]);
    // The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions

    // Everytime the map method loops through the data, it creates "CourseCard" component and then passes the current element in our coursesData array using courseProp
    // Multiple components created through the map method must have a unique key that will help ReactJS identify which components/elements have been changed, added, or removed
    // const courses = coursesData.map(course => {
    //     return(
    //         <CourseCard key={course.id} courseProp={course} />
    //     )
    // })

    return (
        (user.isAdmin)
        ?
        <Navigate to="/admin" />
        :
        <>
            <h1>Courses</h1>
            {courses}
        </>
    )
}