import { useState, useEffect, useContext } from "react";

import { Navigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Login() {

    // Allows us to consume the UserContext object and its values(properties) to use for user validation 
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Allows us to gain access to methods that allows us to redirect to another page
    // const navigate = useNavigate();

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password])

    function authenticate(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        /* 
            Syntax:
                fetch("url", {options})
                .then(res => res.json())
                .then(data => {})

                /users
                /courses
        */
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data.accessToken);

                if (data.accessToken !== undefined) {
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    let timerInterval;
                    Swal.fire({
                        title: "Login Successful",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        // timerProgressBar: true,
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
                } else {
                    Swal.fire({
                        title: "Authentication failed",
                        position: "top",
                        background: "#FFFAE7",
                        showConfirmButton: false,
                        timer: 2000,
                        // timerProgressBar: true,
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
            })

        /* 
            "localStorage" is a property that allows JavaScript sites and application to save key-value pairs in a web browser with no expiration date
            Syntax: localStorage.setItem("propertyName", value);
        */
        // localStorage.setItem("email", email);

        // setUser({
        //     email: localStorage.getItem("email")
        // })

        setEmail('');
        setPassword('');
        // alert("You are now logged in.");

        // Redirect us to home page
        // navigate("/");
    }

    const retrieveUserDetails = (token) => {
        // Token wll be sent as part of the request's header
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                // Change the global "user" state to store the "id" and "isAdmin" property
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    return (
        // Create a conditional statement that will redirect the user to the course page when a user is already logged in
        (user.id !== null)
            ?
            <Navigate to="/courses" />
            :
            <>
                <h1 className="my-5 text-center">Login</h1>
                <Form onSubmit={e => { authenticate(e) }}>
                    <Form.Group className="mb-3" controlId="emailAddress">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="user@mail.com"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>

                    {
                        isActive
                            ?
                            <Button variant="primary" type="submit" id="sumbitBtn">
                                Submit
                            </Button>
                            :
                            <Button variant="danger" type="submit" id="sumbitBtn" disabled>
                                Submit
                            </Button>
                    }
                </Form>
            </>
    )
}